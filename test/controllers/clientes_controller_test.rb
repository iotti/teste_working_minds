require 'test_helper'

class ClientsControllerTest < ActionController::TestCase
  setup do
    @client = Clients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:clients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create Client" do
    assert_difference('Client.count') do
      post :create, Client: { CPF: @client.CPF, Nome: @client.Nome, user_id: @client.user_id }
    end

    assert_redirected_to Client_path(assigns(:client))
  end

  test "should show Client" do
    get :show, id: @client
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @client
    assert_response :success
  end

  test "should update Client" do
    patch :update, id: @client, Client: { CPF: @client.CPF, Nome: @client.Nome, user_id: @client.user_id }
    assert_redirected_to Client_path(assigns(:client))
  end

  test "should destroy Client" do
    assert_difference('Client.count', -1) do
      delete :destroy, id: @client
    end

    assert_redirected_to Clients_path
  end
end
