class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :client, index: true, foreign_key: true
      t.integer :qtd
      t.integer :purchase_id
      t.date :dtPedido

      t.timestamps null: false
    end
  end
end
