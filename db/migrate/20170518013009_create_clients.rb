class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.references :user, index: true, foreign_key: true
      t.string :Nome
      t.string :CPF

      t.timestamps null: false
    end
    add_index :clients, :CPF,                unique: true
  end  
end
