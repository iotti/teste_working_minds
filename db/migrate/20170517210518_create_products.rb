class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :NomeProduct
      t.string :CodBarras, null: false
      t.decimal :ValorUnitario, precision: 10, scale: 2

      t.timestamps null: false
    end
  end
end
