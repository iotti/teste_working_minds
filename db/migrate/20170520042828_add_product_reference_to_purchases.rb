class AddProductReferenceToPurchases < ActiveRecord::Migration
  def change
    add_reference :purchases, :product, index: :true
  end
end
