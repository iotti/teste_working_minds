class AddStatuses < ActiveRecord::Migration
  def change
    Status.create(name:'Em Aberto')
    Status.create(name:'Pago')
    Status.create(name:'Cancelado')
  end
end
