class AddStatusFieldToPurchases < ActiveRecord::Migration
  def change
    add_reference :purchases, :status, index: :true, null: :false
  end
end
