class ProductSearchService

    attr_accessor :p
    def initialize(params)
        self.p = params
    end

    def search
        unless p.blank?
            @search = Product.ransack(p[:q])
            @search.build_condition
            @search.build_grouping unless @search.groupings.any?
            return @search
        end
        Product.ransack
    end
end