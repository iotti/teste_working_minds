json.partial!  partial:'layouts/json_header', locals: {model: Client}
json.result do |j|
    json.array! @clients, partial: 'clients/client', as: :client
end
