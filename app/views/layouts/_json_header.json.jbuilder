json.page params.fetch(:page,1)
json.per_page params.fetch(:per_page,model.count).to_i
json.total_itens model.count
json.total_pages model.count/params.fetch(:per_page,1).to_i