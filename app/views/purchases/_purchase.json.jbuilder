json.extract! purchase, :id, :client, :status, :product, :qtd, :dtPedido, :created_at, :updated_at
json.url purchase_url(purchase, format: :json)
