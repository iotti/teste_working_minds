json.partial!  partial:'layouts/json_header', locals: {model: Purchase}
json.result do |j|
    json.array! @purchases, partial: 'purchases/purchase', as: :purchase
end
