json.partial!  partial:'layouts/json_header', locals: {model: Product}
json.result do |j|
    j.array! @products, partial: 'products/product', as: :product
end
