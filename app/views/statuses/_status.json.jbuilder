json.extract! status, :id, :name
json.url status_url(status, format: :json)
