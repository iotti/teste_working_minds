class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  protect_from_forgery :except => :fetch_products


  # GET /Products
  # GET /Products.json
  def index
    @products = Product.all
    @search = ProductSearchService.new(params).search
    @products = @search.result.paginate(page: params[:page].nil? ? 1 : params[:page], per_page: params[:per_page].nil? ? 10 : params[:per_page])
  end

  # GET /Products/1
  # GET /Products/1.json
  def show
  end

  # GET /Products/new
  def new
    @product = Product.new
  end

  # GET /Products/1/edit
  def edit
  end

  #POST 
  def fetch_products
    p = Product.select('id','NomeProduct as text').limit(8)
    p = p.where(['NomeProduct like ?', params[:term] + '%']) unless params[:term].blank?
    respond_with p
  end

  # POST /Products
  # POST /Products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /Products/1
  # PATCH/PUT /Products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /Products/1
  # DELETE /Products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: "Produto #{@product.id} removido!" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:NomeProduct, :CodBarras, :ValorUnitario)
    end
end
