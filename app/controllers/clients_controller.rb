class ClientsController < ApplicationController
  respond_to :html, :json
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  protect_from_forgery :except => :fetch_clients

  # GET /:clients
  # GET /:clients.json
  def index
    @search = ClientSearchService.new(params).search
    @clients = @search.result.paginate(page: params[:page].nil? ? 1 : params[:page], per_page: params[:per_page].nil? ? 10 : params[:per_page])
  end

  # GET /:clients/1
  # GET /:clients/1.json
  def show; end

  # GET /:clients/new
  def new
    @client = Client.new
  end

  # GET /:clients/1/edit
  def edit; end

  # POST /:clients
  # POST /:clients.json
  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def purchase_clients
  end

  def purchase_client_selected
    session[:purchase_client] = Client.find[client_purchase_params[:id]]
    redirect_to purchase_product_path
  end

  #POST 
  def fetch_clients
    c = Client.select('id','nome as text').limit(8)
    c = c.where(['nome like ?', params[:term] + '%']) unless params[:term].blank?
    respond_with c
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_client
    @client = Client.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def client_params
    params.require(:client).permit(:Nome, :CPF)
  end

  def client_purchase_params
    params.require(:client).permit(:id)
  end

  def list_clients
    @clients = Client.paginate(page: params[:page], per_page: session[:per_page].present? ? session[:per_page] : 20)
  end
end
