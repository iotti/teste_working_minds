class PurchasesController < ApplicationController
  before_action :set_purchase, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  # GET /purchases
  # GET /purchases.json
  def index
    @search = PurchaseSearchService.new(params).search
    @purchases = @search.result.joins(:product).select('Purchases.*, (Purchases.qtd * Products.ValorUnitario) total')
    @purchases = @purchases.order(params.dig(:q,:s)) if params[:q].present?
    @purchases = @purchases.paginate(page: params[:page].nil? ? 1 : params[:page], per_page: params[:per_page].nil? ? 10 : params[:per_page])
  end

  # GET /purchases/1
  # GET /purchases/1.json
  def show
  end

  # GET /purchases/new
  def new
    @purchase = Purchase.new
  end

  # GET /purchases/1/edit
  def edit
  end

  # POST /purchases
  # POST /purchases.json
  def create
    @purchase = Purchase.new(purchase_params)

    respond_to do |format|
      if @purchase.save
        format.html { redirect_to @purchase, notice: 'Pedido compra was successfully created.' }
        format.json { render :show, status: :created, location: @purchase }
      else
        format.html { render :new }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchases/1
  # PATCH/PUT /purchases/1.json
  def update
    respond_to do |format|
      if @purchase.update(purchase_params)
        format.html { redirect_to @purchase, notice: 'Pedido compra was successfully updated.' }
        format.json { render :show, status: :ok, location: @purchase }
      else
        format.html { render :edit }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchases/1
  # DELETE /purchases/1.json
  def destroy
    @purchase.destroy
    respond_to do |format|
      format.html { redirect_to purchases_url, notice: 'Pedido compra was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase
      @purchase = Purchase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params[:dtPedido] = params[:dtPedido_submit]
      puts params.inspect
      params.permit(:client_id,:status_id, :product_id, :qtd, :dtPedido)
    end
end
