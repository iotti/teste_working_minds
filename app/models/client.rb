class Client < ActiveRecord::Base
  belongs_to :user

  def self.search(search)
    if search
      Client.where("Nome LIKE ?", "%#{search}%")
    else
      super
    end
  end
end
